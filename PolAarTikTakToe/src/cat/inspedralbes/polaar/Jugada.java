package cat.inspedralbes.polaar;

public class Jugada {
	private int posicioJugadaX;
	private int posicioJugadaY;
	

	public int[] getPosicioJugada() {
		int [] posicioJugada = new int[2];
		posicioJugada[0] = posicioJugadaX;
		posicioJugada[1] = posicioJugadaY;
		
		return posicioJugada;
	}

	public void setPosicioJugada(int posicioJugadaX, int posicioJugadaY) {
		this.posicioJugadaX = posicioJugadaX;
		this.posicioJugadaY = posicioJugadaY;
	}
	
	
}

package cat.inspedralbes.polaar;

import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.MatteBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JColorChooser;
import javax.swing.JFileChooser;
import javax.swing.KeyStroke;
import java.awt.event.KeyEvent;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import javax.swing.ImageIcon;
import java.awt.event.InputEvent;
import java.awt.CardLayout;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.ButtonGroup;
import javax.swing.JToolBar;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.SystemColor;
import javax.swing.UIManager;

public class TaulerGUI extends JFrame implements ActionListener{
	private static final String JUGADOR1 = "Jugador1";
	private static final String JUGADOR2 = "Jugador2";
	private static final String UBUNTU = "Ubuntu";
	private static final String ERROR = "Error!";
	private static final String GUANYA = "Guanya: ";
	private PrintWriter escriure;
	private JPanel contentPane;
	private Partida partida;
	private JButton c0;
	private JButton c21;
	private JButton c20;
	private JButton c1;
	private JButton c01;
	private JButton c02;
	private JButton c12;
	private JButton c22;
	private JButton c11;
	private JMenuItem mntmNewGame;
	private JMenuItem mntmExit;
	private JMenu mnOptions;
	private JMenu mnSetPlayers;
	private JMenuItem mntmPlayerVsPlayer;
	private JMenuItem mntmPlayerVsCpu;
	private JLabel lblJugador1;
	private JLabel lblJugador2;
	private JMenuItem mntmJugador1;
	private JMenuItem mntmJugador2;
	private JMenuItem mntmJugador11;
	private JMenuItem mntmJugador21;
	private JMenuItem mntmJugador12;
	private JMenuItem mntmJugador22;
	private JMenu mnHelp;
	private JMenuItem mntmAbout;
	private JLabel labelText;
	private Component[] component;
	private JPanel panel = new JPanel();
	private JMenu mnSetIaLevel;
	private final ButtonGroup buttonGroup = new ButtonGroup();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TaulerGUI frame = new TaulerGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TaulerGUI() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				int seleccion = JOptionPane.showOptionDialog(null, "Vols Sortir?", "Exit",
						JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, new Object[] { "Si", "No" },
						"opcion 2");
				if (seleccion == 0) {
					dispose();
				}
			}
		});

		partida = new Partida();
		setResizable(false);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 458, 349);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		// OPCIO FILE MENU
		JMenu mnFile = new JMenu("File");
		mnFile.setMnemonic('F');
		menuBar.add(mnFile);

		// OPCIO NEW GAME
		mntmNewGame = new JMenuItem("New Game");
		mntmNewGame.setMnemonic('N');
		mntmNewGame.setIcon(new ImageIcon(TaulerGUI.class.getResource("/cat/inspedralbes/polaar/img/file.png")));
		mnFile.add(mntmNewGame);

		// OPCIO EXIT
		mntmExit = new JMenuItem("Exit");
		mntmExit.setIcon(new ImageIcon(TaulerGUI.class.getResource("/cat/inspedralbes/polaar/img/door.png")));
		mntmExit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0));
		mntmExit.addActionListener(this);
		
		JMenuItem mntmSaveGamewip = new JMenuItem("Save Game (WIP)");
		mntmSaveGamewip.addActionListener(this);
		mntmSaveGamewip.setActionCommand("GuardarPartida");
		mnFile.add(mntmSaveGamewip);
		
		JMenuItem mntmLoadGamewip = new JMenuItem("Load Game (WIP)");
		mntmLoadGamewip.addActionListener(this);
		mntmLoadGamewip.setActionCommand("CarregarPartida");
		mnFile.add(mntmLoadGamewip);
		mnFile.add(mntmExit);

		mnOptions = new JMenu("Options");
		mnOptions.setMnemonic('O');
		menuBar.add(mnOptions);

		mnSetPlayers = new JMenu("Set Players");
		mnSetPlayers.setIcon(new ImageIcon(TaulerGUI.class.getResource("/cat/inspedralbes/polaar/img/players.png")));
		mnOptions.add(mnSetPlayers);

		mntmPlayerVsPlayer = new JMenuItem("Player Vs Player");
		mntmPlayerVsPlayer.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F2, 0));
		mnSetPlayers.add(mntmPlayerVsPlayer);

		mntmPlayerVsCpu = new JMenuItem("Player Vs CPU");
		mntmPlayerVsCpu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F3, 0));
		mnSetPlayers.add(mntmPlayerVsCpu);
		
		mnSetIaLevel = new JMenu("Set IA Level (WIP)");
		mnOptions.add(mnSetIaLevel);
		
		JRadioButtonMenuItem rdbtnmntmEasy = new JRadioButtonMenuItem("Easy");
		rdbtnmntmEasy.setSelected(true);
		buttonGroup.add(rdbtnmntmEasy);
		mnSetIaLevel.add(rdbtnmntmEasy);
		
		JRadioButtonMenuItem rdbtnmntmHard = new JRadioButtonMenuItem("Hard");
		buttonGroup.add(rdbtnmntmHard);
		mnSetIaLevel.add(rdbtnmntmHard);

		JMenu mnChangeName = new JMenu("Change Name");
		mnChangeName
				.setIcon(new ImageIcon(TaulerGUI.class.getResource("/cat/inspedralbes/polaar/img/newname (1).png")));
		mnOptions.add(mnChangeName);

		// CHANGE NAME PLAYER 1
		mntmJugador1 = new JMenuItem(JUGADOR1);
		mntmJugador1.setActionCommand("NJugador1");
		mntmJugador1.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_1, InputEvent.ALT_MASK));
		mntmJugador1.addActionListener(this);
		mnChangeName.add(mntmJugador1);

		// CHANGE NAME PLAYER 2
		mntmJugador2 = new JMenuItem(JUGADOR2);
		mntmJugador2.setActionCommand("NJugador2");
		mntmJugador2.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_2, InputEvent.ALT_MASK));
		mntmJugador2.addActionListener(this);
		mnChangeName.add(mntmJugador2);

		// CHANGE SYMBOL PLAYER 1
		JMenu mnChangeSymbol = new JMenu("Change Symbol");
		mnChangeSymbol.setActionCommand("ChangeSymbol");
		mnChangeSymbol
				.setIcon(new ImageIcon(TaulerGUI.class.getResource("/cat/inspedralbes/polaar/img/symbol (1).png")));
		mnOptions.add(mnChangeSymbol);

		mntmJugador11 = new JMenuItem(JUGADOR1);
		mntmJugador11.setActionCommand("SJugador1");
		mntmJugador11.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_1, InputEvent.SHIFT_MASK));
		mntmJugador11.addActionListener(this);
		mnChangeSymbol.add(mntmJugador11);

		// CHANGE SYMBOL PLAYER 2
		mntmJugador21 = new JMenuItem(JUGADOR2);
		mntmJugador21.setActionCommand("SJugador2");
		mntmJugador21.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_2, InputEvent.SHIFT_MASK));
		mntmJugador21.addActionListener(this);
		mnChangeSymbol.add(mntmJugador21);
		JMenu mnChangeColor = new JMenu("Change Color");
		mnChangeColor
				.setIcon(new ImageIcon(TaulerGUI.class.getResource("/cat/inspedralbes/polaar/img/colors (1).png")));
		mnOptions.add(mnChangeColor);

		mntmJugador12 = new JMenuItem(JUGADOR1);
		mntmJugador12.setActionCommand("CJugador1");
		mntmJugador12.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_1, InputEvent.CTRL_MASK));
		mntmJugador12.addActionListener(this);
		mnChangeColor.add(mntmJugador12);

		mntmJugador22 = new JMenuItem(JUGADOR2);
		mntmJugador22.setActionCommand("CJugador2");
		mntmJugador22.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_2, InputEvent.CTRL_MASK));
		mntmJugador22.addActionListener(this);
		mnChangeColor.add(mntmJugador22);
		
		mnHelp = new JMenu("Help");
		menuBar.add(mnHelp);
		mntmAbout = new JMenuItem("About");
		mntmAbout.addActionListener(this);
		mnHelp.add(mntmAbout);
		
		JMenuItem mntmHelpOnline = new JMenuItem("Help Online");
		mntmHelpOnline.setActionCommand("helpOnline");
		mntmHelpOnline.addActionListener(this);
		mnHelp.add(mntmHelpOnline);

		contentPane = new JPanel();
		setContentPane(contentPane);
		contentPane.setLayout(null);

		panel.setBounds(0, 66, 441, 219);
		contentPane.add(panel);
		panel.setLayout(new GridLayout(3, 3, 0, 0));

		c0 = new JButton("");
		c0.setBackground(SystemColor.controlHighlight);
		c0.addMouseListener(new MouseActions());
		c0.setFont(new Font(UBUNTU, Font.BOLD, 72));
		c0.setActionCommand("0.0");
		panel.add(c0);
		c0.addActionListener(this);
		c0.setMargin(new Insets(0, 0, 0, 0));
		c0.setPreferredSize(new Dimension(120, 70));
		c0.setBorder(new MatteBorder(0, 0, 2, 2, new Color(0, 0, 0)));
		//c0.setContentAreaFilled(false);

		c1 = new JButton("");
		c1.setBackground(SystemColor.controlHighlight);
		c1.addMouseListener(new MouseActions());
		c1.setFont(new Font(UBUNTU, Font.BOLD, 72));
		c1.setActionCommand("1.0");
		panel.add(c1);
		c1.setMargin(new Insets(0, 0, 0, 0));
		c1.setPreferredSize(new Dimension(120, 70));
		c1.setBorder(new MatteBorder(0, 2, 2, 2, new Color(0, 0, 0)));
		c1.addActionListener(this);
		//c1.setContentAreaFilled(false);

		c20 = new JButton("");
		c20.setBackground(SystemColor.controlHighlight);
		c20.addMouseListener(new MouseActions());
		c20.setFont(new Font(UBUNTU, Font.BOLD, 72));
		c20.setActionCommand("2.0");
		panel.add(c20);
		c20.addActionListener(this);
		c20.setMargin(new Insets(0, 0, 0, 0));
		c20.setPreferredSize(new Dimension(120, 70));
		c20.setBorder(new MatteBorder(0, 2, 2, 0, new Color(0, 0, 0)));
//		c20.setContentAreaFilled(false);

		c01 = new JButton("");
		c01.setBackground(SystemColor.controlHighlight);
		c01.addMouseListener(new MouseActions());
		c01.setFont(new Font(UBUNTU, Font.BOLD, 72));
		c01.setActionCommand("0.1");
		panel.add(c01);
		c01.addActionListener(this);
		c01.setMargin(new Insets(0, 0, 0, 0));
		c01.setPreferredSize(new Dimension(120, 70));
		c01.setBorder(new MatteBorder(2, 0, 2, 2, new Color(0, 0, 0)));
//		c01.setContentAreaFilled(false);

		c11 = new JButton("");
		c11.setBackground(SystemColor.controlHighlight);
		c11.addMouseListener(new MouseActions());
		c11.setFont(new Font(UBUNTU, Font.BOLD, 72));
		c11.setActionCommand("1.1");
		panel.add(c11);
		c11.addActionListener(this);
		c11.setMargin(new Insets(0, 0, 0, 0));
		c11.setPreferredSize(new Dimension(120, 70));
		c11.setBorder(new MatteBorder(2, 2, 2, 2, new Color(0, 0, 0)));
//		c11.setContentAreaFilled(false);

		c21 = new JButton("");
		c21.setBackground(SystemColor.controlHighlight);
		c21.addMouseListener(new MouseActions());
		c21.setFont(new Font(UBUNTU, Font.BOLD, 72));
		c21.setActionCommand("2.1");
		panel.add(c21);
		c21.addActionListener(this);
		c21.setMargin(new Insets(0, 0, 0, 0));
		c21.setPreferredSize(new Dimension(120, 70));
		c21.setBorder(new MatteBorder(2, 2, 2, 0, new Color(0, 0, 0)));
//		c21.setContentAreaFilled(false);

		c02 = new JButton("");
		c02.setBackground(SystemColor.controlHighlight);
		c02.addMouseListener(new MouseActions());
		c02.setFont(new Font(UBUNTU, Font.BOLD, 72));
		c02.setActionCommand("0.2");
		panel.add(c02);
		c02.addActionListener(this);
		c02.setMargin(new Insets(0, 0, 0, 0));
		c02.setPreferredSize(new Dimension(120, 70));
		c02.setBorder(new MatteBorder(2, 0, 0, 2, new Color(0, 0, 0)));
//		c02.setContentAreaFilled(false);

		c12 = new JButton("");
		c12.setBackground(SystemColor.controlHighlight);
		c12.addMouseListener(new MouseActions());
		c12.setFont(new Font(UBUNTU, Font.BOLD, 72));
		c12.setActionCommand("1.2");
		panel.add(c12);
		c12.addActionListener(this);
		c12.setMargin(new Insets(0, 0, 0, 0));
		c12.setPreferredSize(new Dimension(120, 70));
		c12.setBorder(new MatteBorder(2, 2, 0, 2, new Color(0, 0, 0)));
//		c12.setContentAreaFilled(false);

		c22 = new JButton("");
		c22.setBackground(SystemColor.controlHighlight);
		c22.addMouseListener(new MouseActions());
		c22.setFont(new Font(UBUNTU, Font.BOLD, 72));
		c22.setActionCommand("2.2");
		panel.add(c22);
		c22.addActionListener(this);
		c22.setMargin(new Insets(0, 0, 0, 0));
		c22.setPreferredSize(new Dimension(120, 70));
		c22.setBorder(new MatteBorder(2, 2, 0, 0, new Color(0, 0, 0)));
//		c22.setContentAreaFilled(false);

		JPanel panel1 = new JPanel();
		panel1.setBounds(0, 25, 441, 29);
		contentPane.add(panel1);
		panel1.setLayout(null);

		// DISPLAY PLAYERS AND SYMBOLS
		lblJugador1 = new JLabel(partida.getJugador1().getNom() + " " + partida.getJugador1().getFicha());
		lblJugador1.setBounds(47, 12, 95, 15);
		panel1.add(lblJugador1);

		lblJugador2 = new JLabel(partida.getJugador2().getNom() + " " + partida.getJugador2().getFicha());
		lblJugador2.setBounds(333, 12, 82, 15);
		panel1.add(lblJugador2);
		// DISPLAY CURRENT STATE OF THE GAME
		labelText = new JLabel("Torn de: " + partida.getJugador1().getNom());
		labelText.setForeground(Color.MAGENTA);
		labelText.setBounds(163, 12, 189, 15);
		panel1.add(labelText);
		
		JToolBar toolBar = new JToolBar();
		toolBar.setBounds(0, 0, 441, 29);
		contentPane.add(toolBar);
		
		JButton toolbarName1 = new JButton();
		toolbarName1.setToolTipText("Change name of player 1");
		toolbarName1.addActionListener(this);
		toolbarName1.setActionCommand("NJugador1");
		toolbarName1.setIcon(new ImageIcon(TaulerGUI.class.getResource("/cat/inspedralbes/polaar/img/newname (1).png")));
		toolBar.add(toolbarName1);
		
		JButton toolbarName2 = new JButton();
		toolbarName2.setToolTipText("Change name of player 2");
		toolbarName2.addActionListener(this);
		toolbarName2.setActionCommand("NJugador2");
		toolbarName2.setIcon(new ImageIcon(TaulerGUI.class.getResource("/cat/inspedralbes/polaar/img/newname (1).png")));
		toolBar.add(toolbarName2);
		
		JButton toolbarSimbol1 = new JButton();
		toolbarSimbol1.setToolTipText("Change symbol of player 1");
		toolbarSimbol1.addActionListener(this);
		toolbarSimbol1.setActionCommand("SJugador1");
		toolbarSimbol1.setIcon(new ImageIcon(TaulerGUI.class.getResource("/cat/inspedralbes/polaar/img/symbol (1).png")));
		toolBar.add(toolbarSimbol1);
		
		JButton toolbarSimbol2 = new JButton();
		toolbarSimbol2.setToolTipText("Change name of player 2");
		toolbarSimbol2.addActionListener(this);
		toolbarSimbol2.setActionCommand("SJugador2");
		toolbarSimbol2.setIcon(new ImageIcon(TaulerGUI.class.getResource("/cat/inspedralbes/polaar/img/symbol (1).png")));
		toolBar.add(toolbarSimbol2);
		
		JButton toolbarColor1 = new JButton();
		toolbarColor1.setToolTipText("Change color of player 1");
		toolbarColor1.addActionListener(this);
		toolbarColor1.setActionCommand("CJugador1");
		toolbarColor1.setIcon(new ImageIcon(TaulerGUI.class.getResource("/cat/inspedralbes/polaar/img/colors (1).png")));
		toolBar.add(toolbarColor1);
		
		JButton toolbarColor2 = new JButton();
		toolbarColor2.setToolTipText("Change color of player 2");
		toolbarColor2.addActionListener(this);
		toolbarColor2.setActionCommand("CJugador2");
		toolbarColor2.setIcon(new ImageIcon(TaulerGUI.class.getResource("/cat/inspedralbes/polaar/img/colors (1).png")));
		toolBar.add(toolbarColor2);
		
		component = panel.getComponents();

		// ACCIO NEW GAME
		mntmNewGame.addActionListener(this);
		// NEW GAME PLAYER VS PLAYER
		mntmPlayerVsPlayer.addActionListener(this);
		// NEW GAME PLAYER VS CPU
		mntmPlayerVsCpu.addActionListener(this);
	}

	int[] receiveCoordenades(ActionEvent arg0) {
		int[] num = new int[2];
		int x = Integer.parseInt(arg0.getActionCommand().substring(0, 1));
		int y = Integer.parseInt(arg0.getActionCommand().substring(2, 3));
		num[0] = x;
		num[1] = y;
		return num;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		
		switch (arg0.getActionCommand()) {
			//CANVIA COLORS
		case "CJugador1":
			colorChooserPlayer(partida.getJugador1(), partida.getJugador2());
			actualitzaButtonsColors();
			break;

		case "CJugador2":
			colorChooserPlayer(partida.getJugador2(), partida.getJugador1());
			actualitzaButtonsColors();
			break;
			
			//CANVIA SIMBOLS
		case "SJugador1":
			String simbolAntic = partida.getJugador1().getFicha();
			changeSymbol(partida.getJugador1(), partida.getJugador2());
			actualitzaButtonsSymbol(simbolAntic, partida.getJugador1());
			actualitzaLabels();
			break;

		case "SJugador2":
			String simbolAntic2 = partida.getJugador2().getFicha();
			changeSymbol(partida.getJugador2(), partida.getJugador1());
			actualitzaButtonsSymbol(simbolAntic2, partida.getJugador2());
			actualitzaLabels();
			break;
			//CANVIA NOMS
		case "NJugador1":
			changeNamePlayer(partida.getJugador1(), partida.getJugador2());
			actualitzaLabels();
			break;
			//MOSTRA AJUDA EN LINEA
		case "helpOnline":
			 try {
			        Desktop.getDesktop().browse(new URL("https://en.wikipedia.org/wiki/Tic-tac-toe").toURI());
			    } catch (Exception e) {
			        e.printStackTrace();
			    }
			break;

		case "NJugador2":
			changeNamePlayer(partida.getJugador2(), partida.getJugador1());
			actualitzaLabels();
			break;

		case "Exit":
			System.exit(0);
			break;
		case "New Game":
			newGame();
			break;
		case "About":
			AboutDialog about = new AboutDialog();
			about.setVisible(true);
			break;
		case "Player Vs Player":
			for (int i = 0; i < component.length; i++) {
				if (component[i] instanceof JButton) {
					((JButton) component[i]).setText("");
				}
			}
			Color[] colors1 = guardaColors();
			String[] simbols1 = guardaSimbol();
			String[] noms1 = guardaNoms();
			partida = new Partida();
			setColorToPlayers(colors1);
			setNomsToPlayers(noms1);
			setSimbolsToPlayers(simbols1);
			deshabilitaButtons();
			habilitaButtons();
			actualitzaLabels();
			break;
		case "Player Vs CPU":
			for (int i = 0; i < component.length; i++) {
				if (component[i] instanceof JButton) {
					((JButton) component[i]).setText("");
				}
			}
			Color[] colors2 = guardaColors();
			String[] simbols2 = guardaSimbol();
			String[] noms2 = guardaNoms();
			partida = new Partida();
			partida.setTipusPartida(2);
			setColorToPlayers(colors2);
			setNomsToPlayers(noms2);
			setSimbolsToPlayers(simbols2);
			deshabilitaButtons();
			habilitaButtons();
			actualitzaLabels();
			break;
		case "CarregarPartida":
			
			//Llegir fitxer
			String nomFitxer = JOptionPane.showInputDialog(getParent(), "Please enter the name of the file", null);
		    
		    BufferedReader reader;
		    
		    try {
		    	String currentLn;
		    	int contador = 0;
		    	reader = new BufferedReader(new InputStreamReader(new FileInputStream(nomFitxer), StandardCharsets.UTF_16));
		    	while((currentLn = reader.readLine()) != null) {
		    		String [] parts = currentLn.split("\\.");
		    		if(contador == 0) {
		    			System.out.println("La data de la partida es: " + parts[0] + "." + parts[1] + "." + parts[2]);
		    		} else {
//		    			System.out.println("Jugada a casella: "+ parts[0] + "." + parts[1] + "Jugada: "+ parts[2]);
		    			for (int i = 0; i < component.length; i++) {
							if (component[i] instanceof JButton && ((JButton)component[i]).getActionCommand().equals(parts[0]+"."+parts[1])) {
								if(parts[2].equals("1")) {
									((JButton)component[i]).setText(partida.getJugador1().getFicha());
									partida.araJuga(Integer.parseInt(parts[0]), Integer.parseInt(parts[1]));
								}else {
									((JButton)component[i]).setText(partida.getJugador2().getFicha());
									partida.araJuga(Integer.parseInt(parts[0]), Integer.parseInt(parts[1]));
								}
									
							}
							
						}
		    		}
		    		contador++;
		    	}
		    }catch(IOException e) {
		    	e.printStackTrace();
		    }
		    
			break;
			
		case "GuardarPartida":
			String fitxer = JOptionPane.showInputDialog(getParent(), "Please enter the name of the file", null);
			String posicioJugador1 = "";
			String posicioJugador2 = "";
			
			if (fitxer != null) {
				if (!fitxer.endsWith("txt") && !fitxer.endsWith("bin")) {
					JOptionPane.showMessageDialog(null, "File must be .txt or .bin.", ERROR, JOptionPane.ERROR_MESSAGE);
				} else {

					if (fitxer.endsWith("txt")) {
						try {
							escriure = new PrintWriter(
									new OutputStreamWriter(new FileOutputStream(fitxer), StandardCharsets.UTF_16));
							// Guardar dada, [2] = dia [1] = mes [0] = any.
							String date = java.time.LocalDate.now().toString();
							String[] dates = new String[3];

							dates = date.split("-");

							escriure.write(dates[2] + "." + dates[1] + "." + dates[0] + "\n");

							// Guardar caselles.

							for (int i = 0; i < component.length; i++) {
								if (component[i] instanceof JButton) {
									if (((JButton) component[i]).getText().equals(partida.getJugador1().getFicha())) {

										posicioJugador1 = ((JButton) component[i]).getActionCommand();
										posicioJugador1 += ".1";
										escriure.write(posicioJugador1 + "\n");
//										System.out.println(posicioJugador1);
									} else if (((JButton) component[i]).getText()
											.equals(partida.getJugador2().getFicha())) {

										posicioJugador2 = ((JButton) component[i]).getActionCommand();
										posicioJugador2 += ".10";
										escriure.write(posicioJugador2 + "\n");
//										System.out.println(posicioJugador2);

									}
								}
							}
							escriure.close();
						} catch (FileNotFoundException e) {
							e.printStackTrace();
						}
					} else if (fitxer.endsWith("bin")) {
						try {
							DataInputStream in = new DataInputStream(new BufferedInputStream(new FileInputStream(fitxer)));
							
							
							
						} catch (FileNotFoundException e) {
							e.printStackTrace();
						}
					}
				}
			}
			break;
		default:
			JButton button = (JButton) arg0.getSource();

			int[] coords = receiveCoordenades(arg0);

			partida.seleccioJugades(coords[0], coords[1]);
			
			imprimeixSimbol(button, coords[0], coords[1]);
			

			if (partida.getTipusPartida() == 1) {
				actualitzaLabels();
				if (partida.getTorn() == 1) {
					button.setForeground(partida.getJugador1().getColor());
				} else {
					button.setForeground(partida.getJugador2().getColor());
				}
			}

			if (partida.getTipusPartida() == 2) {
				actualitzaLabels();
				button.setForeground(partida.getJugador1().getColor());
			}

			if (partida.getTipusPartida() == 2) {
				actualitzaPanellCpu();
			}

			buscaGuanyador();

			if (partida.isPartidaAcabada()) {
				deshabilitaButtons();
				int opcio = optionPaneFinalGame();
				if(opcio == 0) {
					newGame();
				}else {
					System.exit(1);
				}
			}
		}
	}

	public void actualitzaPanellCpu() {
		int posX = partida.getCpu1().getPosicioJugadaX();
		int posY = partida.getCpu1().getPosicioJugadaY();

		if (posX == 0 && posY == 0) {
			imprimeixSimbol(c0, posX, posY);
			setButtonForegroundCPU(c0);
		} else if (posX == 0 && posY == 1) {
			imprimeixSimbol(c01, posX, posY);
			setButtonForegroundCPU(c01);
		} else if (posX == 0 && posY == 2) {
			imprimeixSimbol(c02, posX, posY);
			setButtonForegroundCPU(c02);
		} else if (posX == 1 && posY == 0) {
			imprimeixSimbol(c1, posX, posY);
			setButtonForegroundCPU(c1);
		} else if (posX == 1 && posY == 1) {
			imprimeixSimbol(c11, posX, posY);
			setButtonForegroundCPU(c11);
		} else if (posX == 1 && posY == 2) {
			imprimeixSimbol(c12, posX, posY);
			setButtonForegroundCPU(c12);
		} else if (posX == 2 && posY == 1) {
			imprimeixSimbol(c21, posX, posY);
			setButtonForegroundCPU(c21);
		} else if (posX == 2 && posY == 0) {
			imprimeixSimbol(c20, posX, posY);
			setButtonForegroundCPU(c20);
		} else if (posX == 2 && posY == 2) {
			imprimeixSimbol(c22, posX, posY);
			setButtonForegroundCPU(c22);
		}
	}

	public void imprimeixSimbol(JButton button, int posX, int posY) {
		if (partida.getTauler().getCasellaIndividual(posX, posY) == 1) {
			button.setText(partida.getJugador1().getFicha());
		} else {
			button.setText(partida.getJugador2().getFicha());
		}
		button.removeActionListener(this);
	}

	public void deshabilitaButtons() {
		c0.removeActionListener(this);
		c21.removeActionListener(this);
		c20.removeActionListener(this);
		c1.removeActionListener(this);
		c01.removeActionListener(this);
		c02.removeActionListener(this);
		c12.removeActionListener(this);
		c22.removeActionListener(this);
		c11.removeActionListener(this);
	}

	public void habilitaButtons() {
		c0.addActionListener(this);
		c21.addActionListener(this);
		c20.addActionListener(this);
		c1.addActionListener(this);
		c01.addActionListener(this);
		c02.addActionListener(this);
		c12.addActionListener(this);
		c22.addActionListener(this);
		c11.addActionListener(this);
	}

	public void changeNamePlayer(Jugador jugador, Jugador jugadorEnemic) {
		String nomAntic = jugador.getNom();
		String nom = JOptionPane.showInputDialog(getParent(), "Enter the new name for " + jugador.getNom(), null);
		if (nom != null) {
			if (nom.equals(jugadorEnemic.getNom())) {
				JOptionPane.showMessageDialog(null, "Name can not be the same as your opponent.", ERROR,
						JOptionPane.ERROR_MESSAGE);
				nom = nomAntic;
				changeNamePlayer(jugador, jugadorEnemic);
			} else if (nom.equals("")) {
				JOptionPane.showMessageDialog(null, "Name cannot be in blank", ERROR, JOptionPane.ERROR_MESSAGE);
				nom = nomAntic;
				changeNamePlayer(jugador, jugadorEnemic);
			} else {
			jugador.setNom(nom);
		}
	}
	}

	public void changeSymbol(Jugador jugador, Jugador jugadorEnemic) {
		String fichaAnterior = jugador.getFicha();
		String ficha = JOptionPane.showInputDialog(getParent(), "Enter the new symbol for " + jugador.getNom(), null);

		if (ficha != null) {
			if (ficha.equalsIgnoreCase(jugadorEnemic.getFicha())) {
				JOptionPane.showMessageDialog(null, "Symbol can not be the same as your opponent.", ERROR,
						JOptionPane.ERROR_MESSAGE);
				ficha = fichaAnterior;
				changeSymbol(jugador, jugadorEnemic);
			} else if (ficha.equals("")) {
				JOptionPane.showMessageDialog(null, "Symbol cannot be in blank", ERROR, JOptionPane.ERROR_MESSAGE);
				ficha = fichaAnterior;
				changeSymbol(jugador, jugadorEnemic);
			} else if (ficha.length() > 1) {
				JOptionPane.showMessageDialog(null, "Symbol can only contain 1 character", ERROR,
						JOptionPane.ERROR_MESSAGE);
				ficha = fichaAnterior;
				changeSymbol(jugador, jugadorEnemic);
			} else {
			jugador.setFicha(ficha);
			}
		}
	}

	public void actualitzaLabels() {
		lblJugador1.setText(partida.getJugador1().getNom() + " " + partida.getJugador1().getFicha());
		mntmJugador1.setText(partida.getJugador1().getNom());
		mntmJugador11.setText(partida.getJugador1().getNom());
		mntmJugador12.setText(partida.getJugador1().getNom());
		
		if(partida.getTipusPartida() == 1) {
		mntmJugador22.setText(partida.getJugador2().getNom());
		mntmJugador21.setText(partida.getJugador2().getNom());
		mntmJugador2.setText(partida.getJugador2().getNom());
		lblJugador2.setText(partida.getJugador2().getNom() + " " + partida.getJugador2().getFicha());
		} else {
			mntmJugador22.setText(partida.getCpu1().getNom());
			mntmJugador21.setText(partida.getCpu1().getNom());
			mntmJugador2.setText(partida.getCpu1().getNom());
			lblJugador2.setText(partida.getCpu1().getNom() + " " + partida.getJugador2().getFicha());
		}
		
		if (partida.getTorn() == 0) {
			labelText.setText("Torn de: " + partida.getJugador1().getNom());
		} else {
			labelText.setText("Torn de: " + partida.getJugador2().getNom());
		}

	}

	public void buscaGuanyador() {
		if (partida.getJugador1().getEsGuanyador()) {
			labelText.setText(GUANYA + partida.getJugador1().getNom() + "!");
		} else if (partida.getJugador2().getEsGuanyador()) {
			labelText.setText(GUANYA + partida.getJugador2().getNom() + "!");
		} else if (partida.getCpu1().getEsGuanyador()) {
			labelText.setText(GUANYA + partida.getCpu1().getNom() + "!");
		} else if (partida.getContador() == 9) {
			labelText.setText("EMPAT!");
		}
	}

	public void colorChooserPlayer(Jugador jugador, Jugador jugadorEnemic) {
		Color newColor = JColorChooser.showDialog(this, "Choose Color", jugador.getColor());
		if (newColor != null && newColor.getRGB() != jugadorEnemic.getColor().getRGB()) {
			jugador.setColor(newColor);
		}
	}

	public Color[] guardaColors() {
		Color[] colors = new Color[2];
		Color colorj1;
		Color colorj2;
		colorj1 = partida.getJugador1().getColor();
		colorj2 = partida.getJugador2().getColor();
		colors[0] = colorj1;
		colors[1] = colorj2;
		return colors;
	}

	public void setColorToPlayers(Color[] color) {
		partida.getJugador1().setColor(color[0]);
		partida.getJugador2().setColor(color[1]);
		}

	public String[] guardaSimbol() {
		String[] simbols = new String[2];
		String simbolj1 = partida.getJugador1().getFicha();
		String simbolj2 = partida.getJugador2().getFicha();
		simbols[0] = simbolj1;
		simbols[1] = simbolj2;
		return simbols;
	}

	public void setSimbolsToPlayers(String[] simbols) {
		partida.getJugador1().setFicha(simbols[0]);
		partida.getJugador2().setFicha(simbols[1]);
	}
	
	public String[] guardaNoms() {
		String[] noms = new String[2];
		String nom1 = partida.getJugador1().getNom();
		String nom2 = partida.getJugador2().getNom();
		noms[0] = nom1;
		noms[1] = nom2;
		return noms;
	}
	
	public void setNomsToPlayers(String[] noms) {
		partida.getJugador1().setNom(noms[0]);
		partida.getJugador2().setNom(noms[1]);
	}
	
	public void setButtonForegroundCPU(JButton button) {
		button.setForeground(partida.getJugador2().getColor());
	}
	
	public void actualitzaButtonsColors() {
		for (int i = 0; i < component.length; i++) {
			if(component[i] instanceof JButton) {
				if(((JButton)component[i]).getText().equals(partida.getJugador1().getFicha())){
					((JButton)component[i]).setForeground(partida.getJugador1().getColor());
				} else if(((JButton)component[i]).getText().equals(partida.getJugador2().getFicha())) {
					((JButton)component[i]).setForeground(partida.getJugador2().getColor());
				}
			}
		}
	}
	
	public void actualitzaButtonsSymbol(String simbolAntic, Jugador jugador) {
		for (int i = 0; i < component.length; i++) {
			if(component[i] instanceof JButton) {
				if(((JButton)component[i]).getText().equals(simbolAntic)){
					((JButton)component[i]).setText(jugador.getFicha());
				} 
			}
		}
	}
	
	public int optionPaneFinalGame() {
		Object[] options = { "Si", "No, tancar aplicació" };
		return JOptionPane.showOptionDialog(getParent(), "Vols una altre partida?", "Partida finalitzada", JOptionPane.DEFAULT_OPTION,
				JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
	}
	
	public void newGame() {
		for (int i = 0; i < component.length; i++) {
			if (component[i] instanceof JButton) {
				((JButton) component[i]).setText("");
			}
		}
		int tipusPartida = partida.getTipusPartida();
		Color[] colors = guardaColors();
		String[] simbols = guardaSimbol();
		String[] noms = guardaNoms();
		partida = new Partida();
		partida.setTipusPartida(tipusPartida);
		setNomsToPlayers(noms);
		setColorToPlayers(colors);
		setSimbolsToPlayers(simbols);
		deshabilitaButtons();
		habilitaButtons();
		actualitzaLabels();
	}
	
	public void L(int X, int Y, int ficha, JButton button) {
		
		
		
	}
	
}

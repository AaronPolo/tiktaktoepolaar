package cat.inspedralbes.polaar;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class AboutDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			AboutDialog dialog = new AboutDialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public AboutDialog() {
		setBounds(100, 100, 450, 340);
		About dialogContent = new About();
		setContentPane(dialogContent);
		dialogContent.setProgramCredits("html/credits.html");
		dialogContent.setProgramLicense("html/license.html");
		dialogContent.setProgramDescription("Tic Tac Toe, joc de 3 en ratlla");
		//dialogContent.setProgramIcon("cat/inspedralbes/polaar/img/tiktaktoe.png");
		dialogContent.setProgramName("PolAar Tic-Tac-Toe");
	}

}

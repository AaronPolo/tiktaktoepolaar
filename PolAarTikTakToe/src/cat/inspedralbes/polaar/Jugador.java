package cat.inspedralbes.polaar;

import java.awt.Color;

public class Jugador {
	private boolean fichaX = false;
	private boolean esGuanyador = false;
	private String nom;
	private String ficha;
	private Color color = Color.black;
	
	public Jugador(String nom, boolean fichaX){
		this.nom = nom;
		this.fichaX = fichaX;
		if(fichaX) {
			ficha = "X";
		}else {
			ficha = "O";
		}
	}

	public boolean getFichaX() {
		return fichaX;
	}
	
	public void setFichaX(boolean fichaX) {
		this.fichaX = fichaX;
	}
	
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public boolean getEsGuanyador() {
		return esGuanyador;
	}
	
	public void setEsGuanyador(boolean esGuanyador) {
		this.esGuanyador = esGuanyador;
	}
	
	public String getNom() {
		return nom;
	}

	public String getFicha() {
		return ficha;
	}

	public void setFicha(String ficha) {
		this.ficha = ficha;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}
}

package cat.inspedralbes.polaar;

/**
 * @author Aaron Polo
 * @custom.documentedBy Aaron Polo
 */
public class Tauler {

	private int[][] casella = new int[3][3];

	/**
	 * Cada vegada que es crea un tauler posa totes les caselles a 0.
	 */
	public Tauler() {
		inicialitzarCaselles();
	}

	/**
	 * Posa totes les caselles a 0
	 */
	public void inicialitzarCaselles() {
		for (int i = 0; i < casella.length; i++) {
			for (int j = 0; j < casella[i].length; j++) {
				casella[i][j] = 0;
			}
		}
	}

	/**
	 * Retorna un array de casella.
	 * @return casella
	 */
	public int[][] getCasella() {
		return casella;
	}

	/**
	 * Retorna els valors de una posició de casella en concret.
	 * @param posicioX La posició horitzontal de la casella
	 * @param posicioY La posició vertical de la casella
	 * @return Els valors de casella[posicioX][posicioY]
	 */
	public int getCasellaIndividual(int posicioX, int posicioY) {
		return casella[posicioX][posicioY];
	}

	
	// Casella INT 1 = X
	// Casella INT 10 = Y
	/**
	 * Guarda un valor al array de caselles.
	 * @param fichaX Identifica si la ficha que es vol guardar es la X o no (per defecte)
	 * @param posicioX La posicio horitzontal de la casella
	 * @param posicioY La posicio vertical de la casella
	 */
	public void setCasella(boolean fichaX, int posicioX, int posicioY) {
		if (fichaX) {
			casella[posicioX][posicioY] = 1;
		} else {
			casella[posicioX][posicioY] = 10;
		}

	}

	/**
	 * Comprova si la jugada es legal, no es poden sobreescriure jugades.
	 * @param jugada La jugada que es vol fer
	 * @return Retorna true si la jugada es pot fer.
	 */
	public boolean comprovaJugadaLegal(Jugada jugada) {
		int[] posicioJugada = jugada.getPosicioJugada();
		boolean legal = false;

		if (casella[posicioJugada[0]][posicioJugada[1]] == 0) {
			legal = true;
		}
		return legal;
	}
	/**
	 * Comprova les 3 columnes per veure si hi ha un guanyador, si una columna es plena del mateix simbol retorna true.
	 * @return Si hi ha guanyador o no
	 */
	public boolean comprovaColumnes() {
		boolean guanyador = false;
		int total = 0;

		
		for (int x = 0; x < casella.length; x++) {
			for (int y = 0; y < casella[x].length; y++) {
				total = total + casella[x][y];
				if (total == 3 || total == 30) {
					guanyador = true;
				}
			}
			total = 0;
		}
		return guanyador;
	}
	/**
	 * Comprova les 3 files per veure si hi ha un guanyador, si una fila es plena del mateix simbol retorna true.
	 * @return True si hi ha guanyador, false si no.
	 */
	public boolean comprovaFiles() {
		boolean guanyador = false;
		int total = 0;

		
		for (int x = 0; x < casella.length; x++) {
			for (int y = 0; y < casella[x].length; y++) {
				total = total + casella[y][x];
				if (total == 3 || total == 30) {
					guanyador = true;
				}
			}
			total = 0;
		}
		return guanyador;
	}
	/**
	 * Comprova la diagonal d'esquerra alt a dreta baix, si la diagonal es plena del mateix simbol torna true
	 * @return True si hi ha guanyador, false si no.
	 */
	public boolean comprovaHoritzonalDreta() {
		boolean guanyador = false;
		int total = 0;
		for (int x = 0; x < casella.length; x++) {
			int y = x;
			total = total + casella[x][y];
			if (total == 3 || total == 30) {
				guanyador = true;
			}
		}
		return guanyador;
	}
	/**
	 * Comprova la diagonal d'esquerra baix a dreta alt, si la diagonal es plena del mateix simbol torna true
	 * @return True si hi ha guanyador, false si no.
	 */
	public boolean comprovaHoritzonalEsquerra() {
		boolean guanyador = false;
		int total = 0;
		int y = 0;
		for (int x = 2; x >= 0; x--) {
			total = total + casella[x][y];
			y++;
			if (total == 3 || total == 30) {
				guanyador = true;
			}
		}
		return guanyador;
	}
	/**
	 * Comprova si hi ha guanyador
	 * @return True si hi ha guanyador, false si no.
	 */
	public boolean comprovaGuanyador(Jugador jugador) {
		boolean guanyador = false;

		if (comprovaColumnes() || comprovaFiles() || comprovaHoritzonalDreta() || comprovaHoritzonalEsquerra()) {
			jugador.setEsGuanyador(true);
			guanyador = true;
		}
		return guanyador;
	}
}

package cat.inspedralbes.polaar;

public class Partida {

	private int torn = 0;
	private boolean partidaAcabada = false;
	private int contador = 0;
	
	
	public int getContador() {
		return contador;
	}

	private int tipusPartida = 1;

	public int getTipusPartida() {
		return tipusPartida;
	}

	private Tauler tauler;
	private Jugador jugador1;
	private Jugador jugador2;
	private CPU cpu1;
	private CPU cpu2;

	public int getTorn() {
		return torn;
	}

	public boolean isPartidaAcabada() {
		return partidaAcabada;
	}

	public Tauler getTauler() {
		return tauler;
	}

	public Jugador getJugador1() {
		return jugador1;
	}

	public Jugador getJugador2() {
		return jugador2;
	}
	
	public CPU getCpu1() {
		return cpu1;
	}

	public CPU getCpu2() {
		return cpu2;
	}


	public void setTorn(int torn) {
		this.torn = torn;
	}

	public void setPartidaAcabada(boolean partidaAcabada) {
		this.partidaAcabada = partidaAcabada;
	}

	public void setTauler(Tauler tauler) {
		this.tauler = tauler;
	}

	public void setJugador1(Jugador jugador1) {
		this.jugador1 = jugador1;
	}

	public void setJugador2(Jugador jugador2) {
		this.jugador2 = jugador2;
	}

	// 1 PVP // 2 PVCPU
	public void setTipusPartida(int tipusPartida) {
		this.tipusPartida = tipusPartida;
	}

	public Partida() {
		tauler = new Tauler();
		jugador1 = new Jugador("Jugador1", true);
		jugador2 = new Jugador("Jugador2", false);
		cpu1 = new CPU("Cpu", false);
	}

	public void seleccioJugades(int posicioX, int posicioY) {

		switch (tipusPartida) {
		case 1:
			araJuga(posicioX, posicioY);
			break;

		case 2:
			araJugaVsCPU(posicioX, posicioY);
			break;

		default:
			break;
		}
	}

	// torn 0 jugador1 //torn 1 jugador2
	public void araJuga(int posicioX, int posicioY) {
		Jugada jugada = new Jugada();

		if (torn == 0) {
			jugada.setPosicioJugada(posicioX, posicioY);
			if (tauler.comprovaJugadaLegal(jugada)) {
				tauler.setCasella(jugador1.getFichaX(), posicioX, posicioY);
				torn++;
				if (tauler.comprovaGuanyador(jugador1)) {
					partidaAcabada = true;
				}
			}

		} else {
			jugada.setPosicioJugada(posicioX, posicioY);
			if (tauler.comprovaJugadaLegal(jugada)) {
				tauler.setCasella(jugador2.getFichaX(), posicioX, posicioY);
				torn = 0;
				if (tauler.comprovaGuanyador(jugador2)) {
					partidaAcabada = true;
				}
			}
		}
		contador++;
		if (contador == 9) {
			partidaAcabada = true;
		}
	}

	public void araJugaVsCPU(int posicioX, int posicioY) {
		Jugada jugada = new Jugada();

		jugada.setPosicioJugada(posicioX, posicioY);
		if (tauler.comprovaJugadaLegal(jugada)) {
			tauler.setCasella(jugador1.getFichaX(), posicioX, posicioY);
			contador++;
			if (tauler.comprovaGuanyador(jugador1)) {
				partidaAcabada = true;
			} else {
				cpu1.jugaCPU(tauler, jugada, this);
				contador++;
				if (tauler.comprovaGuanyador(cpu1)) {
					partidaAcabada = true;
				}
			}
		}
		if (contador == 9) {
			partidaAcabada = true;
		}
	}
}


package cat.inspedralbes.polaar;

import java.awt.Color;
import java.awt.SystemColor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;

public class MouseActions extends MouseAdapter{

	public void mouseEntered(MouseEvent arg0) {
		
		((JButton)arg0.getComponent()).setBackground(Color.blue);
	}
	
	public void mouseExited (MouseEvent arg0) {
		arg0.getComponent().setBackground(SystemColor.controlHighlight);
	}
	
}

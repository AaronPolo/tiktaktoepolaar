package cat.inspedralbes.polaar;

import java.util.Random;

public class CPU extends Jugador {

	private int posicioJugadaX;
	private int posicioJugadaY;
	private Random random = new Random();
	public int getPosicioJugadaX() {
		return posicioJugadaX;
	}

	public int getPosicioJugadaY() {
		return posicioJugadaY;
	}

	public CPU(String nom, boolean fichaX) {
		super(nom, fichaX);
	}
/**
 * 
 * @param tauler
 * @param jugada
 * @param partida
 */
	public void jugaCPU(Tauler tauler, Jugada jugada, Partida partida) {
		boolean trobaCasellaBuida = false;

		while (!trobaCasellaBuida) {
			posicioJugadaX = random.nextInt(3);
			posicioJugadaY = random.nextInt(3);
			if (tauler.getCasellaIndividual(posicioJugadaX, posicioJugadaY) == 0) {
				jugada.setPosicioJugada(posicioJugadaX, posicioJugadaY);
				tauler.setCasella(getFichaX(), posicioJugadaX, posicioJugadaY);
				trobaCasellaBuida = true;
			}
		}

		if (tauler.comprovaGuanyador(this)) {
			partida.setPartidaAcabada(true);
		}
	}
}

